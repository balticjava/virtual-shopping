//stores clients' info to view, shopping items, add/remove. 
package shopping.domain;

import java.util.LinkedHashMap;
import java.util.Map;

public class Cart {

	private int itemCount;
	private static int capacity;
	private Map<Item, Integer> items = new LinkedHashMap<Item, Integer>();
	private String shippingTitle;
	private String shippingAddress;
	private String shippingPhone;
	

	public Cart() {
		itemCount = 0;
		capacity = 10;
	}

	public void add(String itemName, double itemPrice, ItemType type) {
		add(new Item(itemName, itemPrice, type));
	}
	
	public void add(Item item){
		items[itemCount] = item;
		itemCount += 1;
		if (itemCount == capacity) {
			increaseSize();
		}
	}

	public void remove(String itemName) {
		for (int i = 0; i < items.length; i++) {
			Item remove = (Item) items[i];
			if (remove != null)
			if (itemName.equals(remove.getItemName())) {
				items[i] = null;
			}else{
				System.out.println("\n" + "Item " + itemName + " wasn't found.");
			}
		}
		
	}

	private void increaseSize() {
		Item[] item = new Item[capacity + 5];
		for (int i = 0; i < capacity; i++) {
			item[i] = items[i];
		}
		items = item;
		item = null;
		capacity = items.length;
	}
	
	public void printItems() {
		double total = 0;
		for (int i = 0; i < items.length; i++) {
			Item item = items[i];
			if(item == null){
				break;
			}
			total += item.getPrice() * item.getQuantity();
			item.printItem();
		}
		System.out.println("Total: $" + total);
	}	
	public void capacityItems(){
		System.out.println("Capacity: " + capacity + "\n" + "Items: " +  itemCount);
	}
}