package shopping.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OnlineShop extends Cart {
	private List<Cart> carts = new ArrayList<Cart>();
	private List<ItemType> itemTypes = new ArrayList<ItemType>();
	private List<Item> items = new ArrayList<Item>();
	private Client currentClient;
	private UnknownClient unknownClient = new UnknownClient();
	private Map<String, RegisteredClient> registeredClient = new HashMap<String, RegisteredClient>();
}