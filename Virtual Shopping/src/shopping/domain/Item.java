//stores items and its prices. Links to actual items.
package shopping.domain;

public class Item {
	private String name;
	private double price;
	private ItemType itemType;

	public Item(String itemName, double itemPrice, ItemType type) {
		this.name = itemName;
		this.price = itemPrice;
		this.itemType = type;

	}

	public String getItemName() {
		return name;
	}

	public double getPrice() {
		return price;
	}
	
	public ItemType getItemType(){
		return itemType;
	}
	
	public void printItem(){
		//System.out.printf("\n%-10.10d %30s %10.2f %10d %10.2f", id, name, price, quantity, description);
		System.out.println("Item ID: " + "\n" + "Item: " + name + "\n" + 
		"Price: $" + price + "\n" + "Quantity: " + "\n");
	}
}
