//inherits behavior from Client
package shopping.domain;

import java.time.LocalDateTime;

public class RegisteredClient extends Client {
	private String firstName;
	private String lastName;
	private String loginName;
	private String password;
	private String phone;
	private String address;
	private double totalAmountSpent = 0;
	private LocalDateTime lastAccessDate; 
	
	public RegisteredClient (String clientName, String clientSurname){
		firstName = clientName;
		lastName = clientSurname;
	}
	
	
	public String getName(){
		return firstName;
	}
	
	public String getSurname(){
		return lastName;
	}		
}
